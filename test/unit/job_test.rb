require 'test_helper'

class JobTest < ActiveSupport::TestCase

  test "simple jpg to png job" do
    job = Job.create({
      :input_file => test_file('kakapo.jpg'),
      :input_type => 'image/jpg',
      :output_type => 'image/png'
    })
    assert job.valid?
    assert_equal 'new', job.state
    job.run
    #job.sleep <-- testing mode doesn't really work with forking, so this doesn't do anything.
    job.reload
    assert_equal 'finished', job.state
    assert_equal 'success', job.status
    assert File.exists?(job.output_file)
    assert `file #{job.output_file}` =~ /PNG/, "this should contain PNG: #{`file #{job.output_file}`}"
    job.destroy
    assert !File.exist?(job.output_file)
  end

  test "with state" do
    finished_jobs = []
    running_jobs = []
    2.times do 
      finished_jobs << create_job(:state => 'finished') 
    end
    ['new', 'running'].each do |state|
      running_jobs << create_job(:state => state)
    end
    assert_equal Job.with_state('running'), running_jobs 
    assert_equal Job.with_state('finished'), finished_jobs
  end

  def create_job(options)
    options = {
      :input_type => 'image/jpeg', 
      :output_type => 'image/gif',
      :state => 'new'
    }.merge!(options)
    job = Job.create(options)
    job.save!
    job
  end

end
