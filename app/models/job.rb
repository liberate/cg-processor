#
# Job -- a media processing job
#

require 'fileutils'

class Job < ActiveRecord::Base

  has_many :log_entries, :dependent => :delete_all

  serialize :options

  validates_presence_of :input_type, :output_type

  named_scope :unfinished, :conditions => ['state != "finished"']

  before_destroy :check_state_for_destroy

  after_destroy :cleanup_files

  ##
  ## STATES
  ##
 
  STATES = {
   'new'        => ['queued'],
   'queued'     => ['processing'],
   'processing' => ['failure', 'success'],
   'failure'    => ['finished', 'queued'],
   'success'    => ['finished'],
   'finished'   => ['queued']
  }

  def set_state(new_state)
    if STATES[state].include?(new_state)
      update_attribute(:state, new_state)
      log_state
      send("do_#{state}")
    else
      raise "illegal state transition: %s to %s" % [state, new_state]
    end
  end

  ##
  ## JOB CONTROL
  ##

  #
  #
  def run
    set_state('queued')
  end

  def update_attributes(params)
    if params.has_key?('state')
      set_state(params['state'])
    end
    super(params)
  end

  #
  # true if this job is processing something
  #
  def running?
    Job.connection.select_value("SELECT state from jobs where id = #{id}") != 'finished'
  end

  def sleep
    wait(@spawn_id)
  end

  ##
  ## FINDERS
  ##
  named_scope :with_state, lambda { |state|
    return {} if state.nil?
    return {:conditions => 'state != "finished" and state != "failure"'} if state == 'running'
    {:conditions => {:state => state}}
  }

  ##
  ## state machine hooks
  ##

  protected

  #
  # for now, there is no queue, we just run in a forked process.
  # eventually, if we have too many requests for this to make sense,
  # we should add a queue library.
  #
  def do_queued
    @spawn_id = spawn do
      set_state('processing')
    end
  end

  #
  # callback for entry into :process state.
  # this method does the work of processing the job. this is done in its own thread.
  #
  # it can take a really long time, and can update the database, but it
  # cannot access share memory.
  #
  def do_processing
    prepare
    transmog = new_transmogrifier
    status = transmog.run do |progress, state|
      log progress if state.nil?
      log_error progress if state == 'ERROR'
    end
    log_status status
    if status == :success
      set_state('success')
    else
      set_state('failure')
    end
  end

  #
  # callback for entry into failure state
  #
  def do_failure
    update_attribute(:status, 'failure')
    if failure_callback_url
      # ...
    end
    set_state('finished')
  end

  #
  # callback for entry into success state
  #
  def do_success
    update_attribute(:status, 'success')
    save
    if success_callback_url
      # ....
    end
    set_state('finished')
  end

  #
  # callback for entry into finished state
  #
  def do_finished
    update_attribute(:finished_at, Time.now)
  end

  ##
  ## log history
  ##

  public 

  def log(msg, options = {})
    puts msg
    log_entries.create({:text => msg}.merge(options))
  end

  def log_state
    log('state: ' + self.state, :flag => LogEntry::STATE)
  end

  def log_error(msg)
    log(msg, :flag => LogEntry::ERROR)
  end

  def log_status(status)
    case status
      when :success   then log('command exit status: success')
      when :failure   then log('command exit status: failure')
      when :not_found then log('command exit status: command not found')
      when :error     then log('command exit status: error')
    end
  end

  #
  # grabs a blob from upload and saves it locally
  #
  def save_upload_to_file(upload)
    write_attribute(:input_file, new_storage_filename(self.id, self.input_type))
    write_attribute(:output_file, new_storage_filename(self.id, self.output_type))
    File.open(self.input_file, 'wb') { |f| f.write(upload['datafile'].read) }
    self.save!
    self.run
  end


  ##
  ## CLASS METHODS
  ##

  public

  #
  # ensures the file storage directory exists
  #
  def self.create_file_storage
    unless File.directory?(JOB_FILE_STORAGE)
      FileUtils.mkdir_p JOB_FILE_STORAGE
    end
  end

  ##
  ## PRIVATE METHODS
  ##

  private

  #
  # fetch the appropriate transmogrifier to handle the processing for this job
  #
  def new_transmogrifier
    job_options = {
      :input_file => input_file, :input_type => input_type,
      :output_file => output_file, :output_type => output_type
    }
    job_options.merge!(options) if self.options
    return Media.transmogrifier(job_options)
  end

  #
  # prepares this job for processing by loading the required data and setting up the
  # local file storage.
  # 
  # may take a long time, because it might need to fetch and store locally some
  # remote data.
  #
  def prepare
    if input_url and input_file.nil?
      self.input_file = new_storage_filename(id, input_type)
      self.save!
      save_url_to_file(input_url, input_file)
    end
    if output_file.nil?
      self.output_file = new_storage_filename(id, output_type)
      self.save!
    end
  end

  #
  # we need to keep the input and output files around until the Job is destroyed.
  # they are not just tempfiles, because we might need them at a later date.
  #
  def cleanup_files
    unless RAILS_ENV == 'test'
      if File.exists?(input_file)
        File.unlink(input_file)
      end
    end
    if File.exists?(output_file)
      File.unlink(output_file)
    end
  end

  # 
  # currently we do not support destroying a job that is processing
  #
  def check_state_for_destroy
    raise 'You cannot destroy a job that is in the middle of processing' if self.state == 'processing'
  end

  #
  # grabs a blob from a remote url and saves it locally
  #
  def save_url_to_file(url, filename)
    log "starting: %s" % url
    # todo
    raise 'not implemented'
    log "finished: %s" % url
  end

  #
  # creates a new filename
  #
  def new_storage_filename(id, input_type)
    Job.create_file_storage
    "%s/%s-%s-%s" % [JOB_FILE_STORAGE, Time.now.strftime("%Y-%m-%d"), input_type.gsub('/','-'), id] 
  end

  #
  # resolves a full file path given a filename that we store in the db.
  # this is in case the storage location changes, we don't invalidate all the db records.
  #
  #def filepath(filename)
  #  if filename.starts_with?('/')
  #    filename
  #  else
  #    "%s/%s" % [JOB_FILE_STORAGE, filename] 
  #  end
  #end
  #

end

