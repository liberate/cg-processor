#!/usr/bin/env python

""" Stub computation manager script in python

A computation manager for cg-server is a script that accepts two
command-line arguments, the path for an input file and the path for an
output file, and does some computation on the input file to produce
the output file.

A computation manager produces information on the progress of the
computation which is printed to standard out, and exits when the
computation is complete (which is an important detail when the
computation is distributed over a cluster).

Example
-------

$ computation_manager_stub.py infile.txt outfile.txt
Reading infile.txt
Read 10 lines
Pretending to process file
1 second has elapsed
2 seconds have elapsed
3 seconds have elapsed
4 seconds have elapsed
5 seconds have elapsed
Writing output file
Computation Complete
"""

import sys
import time


# parse command line arguments
assert len(sys.argv) == 3, 'usage: computation_manager_stup.py infile outfile'
infile = sys.argv[1]
outfile = sys.argv[2]


# read the input file
print 'Reading %s' % infile
indata = open(infile).readlines()
print 'Read %d lines' % len(indata)


# conduct the computation
print 'Pretending to process file'
time.sleep(1)
print '1 second has elapsed'
for i in [2,3,4,5]:
    time.sleep(1)
    print '%d seconds have elapsed' % i


# write output file
print 'Writing output file'
open(outfile, 'w').write('test output file')

# conclude
print 'computation complete'
sys.exit(0)
